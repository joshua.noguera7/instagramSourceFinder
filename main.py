from selenium import webdriver
from selenium.common.exceptions import TimeoutException
import time

from bs4 import BeautifulSoup
from secrets import email, password


def manual(soup):
	while True:
		try:
			eval(input("?"))
		except:
			pass


def processMeme(soup, url):
	try:
		if soup.find("div", class_="_97aPb").find("img").get("alt"):
			f = open("output.txt", "a")
			f.write(url + " && " + soup.find("div", class_="_97aPb").find("img").get("alt") + "\n")
			f.close()
		else:
			f = open("output.txt", "a")
			f.write(url + " is probably a video \n")
			f.close()
	except:
		pass


driver = webdriver.Firefox()

driver.get("https://www.instagram.com/accounts/login/")
time.sleep(3)
driver.find_element_by_name('username').send_keys(email)
driver.find_element_by_name('password').send_keys(password)
driver.find_element_by_name('password').send_keys(u'\ue007')
time.sleep(2)
input("press enter when meme to start on is ready")

processMeme(BeautifulSoup(driver.page_source), driver.current_url)

while True:
	driver.find_element_by_tag_name('body').send_keys(u'\ue014')
	time.sleep(4)
	processMeme(BeautifulSoup(driver.page_source), driver.current_url)
