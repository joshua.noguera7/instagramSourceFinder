This is a quick tool I made to get alt tags from instagram pages to assist research.
To set up this tool, replace the template strings in secret.py with a username and password for an instagram account you want to use.
To make sure you have the correct libraries to use this tool run "pip install -r requirements.txt" while in the project directory.

To use this tool, run main.py. It will open a webbrowser where you can then change the url to a user's page then click on a meme on their page. From there press enter on the python window and it will start outputing the information to a file called output.txt. It is a good idea to rename this file upon completion so that new information doesn't get appened to the end.